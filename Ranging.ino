/*
 * Connect and initialize ranging devices
 */
void rangingInit() {
  printStatus("Init: Ranging");
  for(int i=0; i < sizeof(rangingDevices)/sizeof(rangingDevices[0]); i++) {
    int rx = rangingDevices[i].rx;
    int tx = rangingDevices[i].tx;
    rangingDevices[i].device.begin(115200, SERIAL_8N1, rx, tx, false);
    rangingDevices[i].response = "";
    rangingDevices[i].status = WD_INIT;
  }
}

/*
 * Reset ranging devices.
 * 
 * Send:
 * - ATE0 (Don't output commands sent)
 * - AT+CWMODE_CUR (Set to station mode for current connection)
 */
void rangingResetDevices() {

  int doneCounter = 0;
  int devicesAmount = sizeof(rangingDevices)/sizeof(rangingDevices[0]);

  if (initFinished) {
    connectionStatus = READY;
    return;
  }
  
  for(int i=0; i < devicesAmount; i++) {

    if (rangingDevices[i].status == WD_READY) {
      doneCounter++;
      continue;
    }
    
    rangingRead(i);
    
    if (rangingDevices[i].status == WD_INIT) {
      rangingWrite(i, "ATE0");
      rangingDevices[i].status = WD_ATE_SENT;
      printStatus("Ranging init: ATE0 sent: ", i);
    }
    else if (rangingDevices[i].status == WD_ATE_SENT && rangingDevices[i].busy == false) {
      rangingWrite(i, "AT+CWMODE_CUR=1");
      rangingDevices[i].status = WD_CWMODE_SENT;
      printStatus("Ranging init: AT+CWMODE_CUR sent: ", i);
    }
    else if (rangingDevices[i].status == WD_CWMODE_SENT && rangingDevices[i].busy == false) {
      rangingDevices[i].status = WD_READY;
      printStatus("Ranging init completed: ", i);
    }
  }

  if (doneCounter == devicesAmount) {
    printStatus("Ranging devices ready");
    connectionStatus = RANGING_DEVICES_READY;
  }

}

/*
 * Write to ranging device
 */
void rangingWrite(int i, const char *cmd) {
  rangingDevices[i].response = "";
  rangingDevices[i].busy = true;
  rangingDevices[i].device.println(cmd);
}

/*
 * Reads returns from ranging devices
 * 
 * Goes to `atTermination`, then saves everything into `response`.
 */
boolean rangingRead(int i) {

  if (rangingDevices[i].busy == false) {
    return false;
  }

  // Read response
  boolean terminated = false;
  while (rangingDevices[i].device.available() > 0) {
    char c = rangingDevices[i].device.read();

    if (terminated == false) { // Don't continue if true, otherwise it won't ever be terminated.
      rangingDevices[i].response += c;
    }

    // Check if response terminated
    terminated = true;
    for(int n=strlen(atTermination)-1; n>=0; n--) {
      if (rangingDevices[i].response.charAt(rangingDevices[i].response.length()-strlen(atTermination)+n) != atTermination[n]) {
        terminated = false;
        break;
      }
    }
  }

  // Reset
  if (terminated) {
    //Serial.println(rangingDevices[i].response);
    rangingDevices[i].response.remove(rangingDevices[i].response.length()-strlen(atTermination)-2);
    rangingDevices[i].busy = false;
    return true;
  }
  return false;
}
