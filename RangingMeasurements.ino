/*
 * TODO:
 * - Timeout
 * - scan everything if timeout
 */


/*
 * Read Signal Strengths
 */
void rangingMeasurementsProcess() {

  static int apc = -1; // AccessPointCounter
  int devicesAmount = sizeof(rangingDevices)/sizeof(rangingDevices[0]);
  for(int i=0; i < devicesAmount; i++) {

    // I'm looking for the switch from busy = true to false to know when it's done.
    // Otherwise this wouldn't be necessary.
    if (rangingDevices[i].busy == true) {
      rangingRead(i);
      if (rangingDevices[i].busy == false) {
        int signalStrength = rangingMeasurementsGetSignalStrength(rangingDevices[i].response);
        if (signalStrength < 0) {
          //accessPoints[rangingDevices[i].accessPointId].data.push(signalStrength);
          //if (accessPoints[rangingDevices[i].accessPointId].data.length() > 0) {
          //  accessPoints[rangingDevices[i].accessPointId].data += ",";
          //}
          //accessPoints[rangingDevices[i].accessPointId].data += String(signalStrength);
          //char signalStrength[4];
          //sprintf(signalStrength, "%d", signalStrengthNum);
          //dataStoreAdd('r', accessPoints[rangingDevices[i].accessPointId].mac, signalStrength, true);
        }
      }
    }
    if (rangingDevices[i].busy == false) {
      do {
        apc++;
        if (apc >= accessPointSize) { apc = 0; }
      } while (accessPoints[apc].active == false);

      rangingDevices[i].accessPointId = apc;
      
      int cmdLength = 15 + strlen(accessPoints[apc].ssid) + strlen(accessPoints[apc].mac) + rangingGetDigits(accessPoints[apc].ch) + 1;
      char command[cmdLength+1];
      char channel[rangingGetDigits(accessPoints[apc].ch)];
      sprintf(channel, "%d", accessPoints[apc].ch);
      strcpy(command, "AT+CWLAP=\"");
      strcat(command, accessPoints[apc].ssid);
      strcat(command, "\",\"");
      strcat(command, accessPoints[apc].mac);
      strcat(command, "\",");
      strcat(command, channel);
      rangingWrite(i, command);
    }
  }  
}

/*
 * Based on all the signal strengths collected, create JSON to be sent through Websocket
 */
char *rangingMeasurementsPrepareJson() {

  // First, get length
  int len = 2; // First and last bracket, ending \0 omitted because one comma too much below
  int lastActive;
  bool dataAvailable = false;
  for(int i=0; i<accessPointSize; i++) {
    if (accessPoints[i].active) {
      lastActive = i;
      len += 6;
      len += strlen(accessPoints[i].mac);
      len += accessPoints[i].data.length();
      dataAvailable = true;
    }
  }

  if (!dataAvailable) {
    return 0;
  }


  // Fill in 
  char stats[len];
  strcpy(stats, "{");
  for(int i=0; i<accessPointSize; i++) {
    if (accessPoints[i].active) {

      int measurementsLength = accessPoints[i].data.length() + 1;
      char measurements[measurementsLength];
      accessPoints[i].data.toCharArray(measurements, measurementsLength);
      accessPoints[i].data = "";
      
      strcat(stats, "\"");
      strcat(stats, accessPoints[i].mac);
      strcat(stats, "\":[");
      strcat(stats, measurements);
      strcat(stats, "]");
      if (i < lastActive) {
        strcat(stats, ",");
      }
    }
  }
  strcat(stats, "}");

  //Serial.println("Created 'data'");
  char *data = (char*)malloc(strlen(stats)+1);
  strcpy(data, stats);

  return data;
}

int rangingMeasurementsGetSignalStrength(String response) {
  int startIndex = response.indexOf(',', 11)+1;
  int endIndex = startIndex+3;
  response = response.substring(startIndex, endIndex);
  return response.toInt();
  
}

int rangingGetDigits(int num) {
   if ( num < 10 )
      return 1;
   if ( num < 100 )
      return 2;
}
