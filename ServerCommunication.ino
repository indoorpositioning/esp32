/*
 * Start Websocket
 * 
 * Send 10 requests to the server that have to be returned. Once this works,
 * the connection tends to be very stable. It may take a little bit of time
 * to get to this point though.
 */
bool serverCommunicationWebsocketStart() {
  if (websocketFlowControl.receivedId < 10) {
    if (websocketQueue.isEmpty() && websocketFlowControl.sentId < 10) {
      serverCommunicationSend("", 'i');
    }
    serverCommunicationProcess();
    return false;
  }
  else {
    printStatus("Websocket successfully started");
    return true;
  }
}

/*
 * Test websocket
 */
void serverCommunicationWebsocketTest() {
  if (websocketQueue.isEmpty()) {
    //serverCommunicationSend("{\"UPC0040120\":[10,20,30]}", 'r');
    //serverCommunicationSend("{\"t\":\"i\",\"d\":{\"location\":{\"type\":\"wifi\",\"devices\":[\"UPC247434185\",\"UPC0040120\",\"UPC Wi-Free\",\"UPC241099055\",\"UPC Wi-Free\",\"devolo-000B3BED75E4\",\"DIRECT-iR\",\"UPC6894357\",\"obi-wlan-kenobi\",\"cut-52331\",\"UPC242803555\",\"Netgear_WG_2G_EXT\",\"Netgear_WZ_2G\",\"devolo-3a5\"]},\"upload_id\": \"esp32_1\",\"type\":\"upload\",\"capabilities\":[\"wifi\"]}}");
  }
}

/*
 * Send a new message to the server.
 */
void serverCommunicationSend(String msg, char type) {
  WebsocketMessage message = {
    .message = msg,
    .type = type
  };
  websocketQueue.push(message);
}

void serverCommunicationSend(char* msg, char type) {
  serverCommunicationSend(String(msg), type);
}

void serverCommunicationSend(String msg) {
  serverCommunicationSend(msg, 'd');
}

/*
 * Fetches relevant data from ranging (and imu) to be sent
 */
void serverCommunicationFetch() {
  bool enoughTimeSinceLastTransmission = (millis() - websocketFlowControl.timeLastSend) > 2*websocketMinTimeBetweenTransmissions;

  if (enoughTimeSinceLastTransmission) {
    if (rangingMeasurements) {
      char *data = rangingMeasurementsPrepareJson();
      if (data) {
        serverCommunicationSend(data, 'r');
      }
      free(data);
    }
    else {
      //dataStoreSend();
    }
    
  }
  serverCommunicationProcess();
}

/*
 * Perform the actual sending and receiving, as well as supportive measures
 * such as flow control.
 */
void serverCommunicationProcess() {
  _serverCommunicationRead();
  _serverCommunicationProcessQueue();
}

/************ Helper functions from here on **************/

void _serverCommunicationProcessQueue() {

  if (!wifiClient.connected()) {
    serverConnectionWifiRestart();
    return;
  }

  bool enoughTimeSinceLastTransmission = (millis() - websocketFlowControl.timeLastSend) > websocketMinTimeBetweenTransmissions;
  bool confirmationReceived = websocketFlowControl.sentId == websocketFlowControl.receivedId;

  /*
  Serial.print(millis());
  Serial.print(" | ");
  Serial.print(websocketFlowControl.timeLastSend);
  Serial.print(" | ");
  Serial.print(millis() - websocketFlowControl.timeLastSend);
  Serial.print(" | ");
  if (confirmationReceived) { Serial.print("c"); } else { Serial.print("n"); }
  Serial.print(" | "); 
  if (websocketQueue.isEmpty()) { Serial.print("e"); } else { Serial.print("n"); }
  Serial.print(" | "); 
  if (dataStoreEmpty()) { Serial.println("e"); } else { Serial.println("n"); }
  */
 
  //Serial.print("|");
  if ((confirmationReceived && websocketFlowControlActive) || (!websocketFlowControlActive)) {
    if (enoughTimeSinceLastTransmission && (!dataStoreEmpty() || !websocketQueue.isEmpty())) {
    //if (enoughTimeSinceLastTransmission && !websocketQueue.isEmpty()) {
      //Serial.println("======================");

      if (!dataStoreEmpty() && websocketQueue.isEmpty()) {
        if (rangingMeasurements) {
          if (rangingEnhanced) {
            dataStoreSend('e');
          }
          else {
            dataStoreSend('r');
          }
        }
        else {
          dataStoreSend();
        }
      }
      //Serial.println("=============");
      websocketFlowControl.timeInitialSend = websocketFlowControl.timeLastSend = millis();    
      WebsocketMessage msg = websocketQueue.pop(); // TODO: websocketQueue is an unnecessary detour at this point
      Serial.print(msg.type);
      Serial.print(": ");
      Serial.println(msg.message);
      _serverCommunicationPrepareMessage(msg.message, msg.type);
      webSocketClient.sendData(msg.message, WS_OPCODE_TEXT, false);
      websocketFlowControl.message = msg.message;
    }
  }
  else if (enoughTimeSinceLastTransmission) {
    int timeSinceInitialSend = millis() - websocketFlowControl.timeInitialSend;
    if (timeSinceInitialSend > websocketFlowControlHardAttemptsLimit) {
      serverConnectionWifiRestart();
      websocketFlowControl.timeInitialSend = websocketFlowControl.timeLastSend = millis();
    }
    else if (timeSinceInitialSend > websocketFlowControlSoftAttemptsLimit) {
      webSocketClient.sendData(websocketFlowControl.message, WS_OPCODE_TEXT, false);
      websocketFlowControl.timeLastSend = millis();
    }
  }
  
}

void _serverCommunicationRead() {
  String data;
  webSocketClient.getData(data);
  if (data.length() > 0) {
    
    DynamicJsonBuffer jsonBuffer;
    JsonObject& root = jsonBuffer.parseObject(data);

    // Flow Control
    int c = root["c"];
    if (c > websocketFlowControl.receivedId) {
      websocketFlowControl.receivedId = c;
      Serial.print("Received: "); Serial.println(c);
    }

    // Init: Received access points to look out for
    const char* t = root["t"];
    if (t && (strcmp(root["t"], "i") == 0) && (root["d"]["wifi"])) {
      accessPointSize = root["d"]["wifi"].size();

      //realloc(accessPoints, accessPointSize*sizeof(AccessPoint));
      for(int n=0; n<accessPointSize; n++) {
        strcpy(accessPoints[n].mac, root["d"]["wifi"][n]);
      }
      
    }


  }
}

void _serverCommunicationPrepareMessage(String &message, char type) {
  websocketFlowControl.sentId++;

  if (message.length() > 0) {
    if (websocketFlowControlActive) {
      message += "}";
      message = "\",\"d\":" + message;
      message = type + message;
      message = ",\"t\":\"" + message;
      message = websocketFlowControl.sentId + message;
      message = "{\"c\":" + message;
    }
    else {
      message += "}";
      message = "\",\"d\":" + message;
      message = type + message;
      message = "{\"t\":\"" + message;
    }
  }
  else {
    message = "{\"c\":";
    message += websocketFlowControl.sentId;
    message += ",\"t\":\"";
    message += type;
    message += "\"}";
  }
}
