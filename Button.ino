void buttonCheckPressed() {
  buttonDownNow = !(bool)touchRead(T1);
  if (buttonDownLast && !buttonDownNow) {
    buttonValue++;
  }
  buttonDownLast = buttonDownNow;
}
