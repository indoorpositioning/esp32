#include <WiFi.h> // ESP32 Wifi
#include <WebSocketClient.h> // Websocket Client (https://github.com/bhagman/Arduino-Websocket-Fast)
#include <HardwareSerial.h> // ESP8266 (ESP32 HardwareSerial)
#include <ArduinoJson.h> // JSON (internal, by Benoit Blanchon)
#include <QueueList.h>
#include "time.h"
#include <MPU9250_asukiaaa.h>

#include "init.h"

void setup() {

  // Init Serial
  debugInit();

  // Establish connection to server
  serverConnectionEstablish();

  if (connectionStatus == WEBSOCKET_INITIALIZED) {
    // Time
    timeConfig();
  
    // Imu Init
    imuInit();
  
    // Init Ranging
    rangingInit();
  }

}

void loop() {
  
  if (connectionStatus == WEBSOCKET_INITIALIZED) {
    bool websocketReady = serverCommunicationWebsocketStart();
    if (websocketReady) connectionStatus = WEBSOCKET_READY;
  }
  else if (connectionStatus == WEBSOCKET_READY) {
    rangingResetDevices();
  }
  else if (connectionStatus == RANGING_DEVICES_READY) {
    rangingFindAccessPoints();
  }
  else if (connectionStatus == SERVER_CONNECTION_INIT) {
    serverCommunicationInitProcess();
  }
  else if (connectionStatus == FIND_CHANNELS) {
    rangingFindAccessPoints();
  }
  else if (connectionStatus == READY) {
    if (!rangingMeasurements) {
      imuUpdate();
    }
    buttonCheckPressed();
    rangingUpdateProcess();
    serverCommunicationProcess();
  }  

}


