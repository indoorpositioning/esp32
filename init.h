/***************/
/*** General ***/
/***************/

// Mode - false for normal mode
boolean rangingMeasurements = false;
boolean rangingEnhanced = false;

// Identifier
char identifier[] = "esp32_1";

// Connection Status
enum ConnectionStatusOptions {
  START,
  WEBSOCKET_INITIALIZED,
  WEBSOCKET_READY,
  RANGING_DEVICES_READY,
  SERVER_CONNECTION_INIT,
  FIND_CHANNELS,
  READY,
  ERROR
};
ConnectionStatusOptions connectionStatus = START;

// Used to determine how much to reinitalize when reconnection needed
bool initFinished = false;

// Debug
int baudRate = 115200;
boolean printStatusMessages = true;
boolean printErrorMessages = true;

/*******************/
/*** Wifi Access ***/
/*******************/

struct WifiAccess {
    const char* ssid;
    const char* password;
};

WifiAccess wifiAccess[] = {
  {
    .ssid = "UPC1000",
    .password = "SAALUCWQa0"
  },
  {
    .ssid = "TwoKeys",
    .password = "0123456789"
  }
};

// Id of wifi access. Find once and then keep until reset.
int wifiAccessId = -1;

// Behavior when trying to connect to a wifi access point
int wifiConnectionAttempts = 30;
int wifiConnectionDelay = 500;

// Clients
WiFiClient wifiClient;
WebSocketClient webSocketClient;

/*********************/
/*** Server Access ***/
/*********************/

struct LocalizationServer {
  const char* address;
  int port;
  const char* websocketPath;
};

LocalizationServer localizationServers[] = {
  {
    .address = "172.20.10.2", // Patrick Mac
    .port = 8000,
    .websocketPath = "/"
  },
  {
    .address = "192.168.43.189",
    .port = 8000,
    .websocketPath = "/"
  },
  {
    .address = "192.168.43.227",
    .port = 8000,
    .websocketPath = "/"
  },
  {
    .address = "192.168.0.100",
    .port = 8000,
    .websocketPath = "/"
  },
  {
    .address = "130.92.70.176",
    .port = 8000,
    .websocketPath = "/"
  }
};

// Id of localizationServer.
int localizationServerId = -1;

/*****************/
/*** Websocket ***/
/*****************/

struct WebsocketMessage {
  String message;
  char type;
};

struct WebsocketFlowControl {
  int sentId;
  int receivedId;
  unsigned long timeInitialSend;
  unsigned long timeLastSend;
  String message;
};

WebsocketFlowControl websocketFlowControl = {
  .sentId = 0,
  .receivedId = 0,
  .timeInitialSend = 0,
  .timeLastSend = 0
};

// Time between websocket transmissions in ms
int websocketMinTimeBetweenTransmissions = 100;

// Sends same message again from here on
int websocketFlowControlSoftAttemptsLimit = 1000;

// Connects again when reached
int websocketFlowControlHardAttemptsLimit = 4000;

// Flow Control
bool websocketFlowControlActive = false;

// Hold websocket commands to be sent
QueueList<WebsocketMessage> websocketQueue;

/***************/
/*** Ranging ***/
/***************/

enum RangingDeviceStatus {
  WD_INIT,
  WD_ATE_SENT,
  WD_CWMODE_SENT,
  WD_READY,
  WD_BUSY
};

struct RangingDevice {
  HardwareSerial device;
  int rx;
  int tx;
  String response;
  boolean busy;
  RangingDeviceStatus status;
  int accessPointId; // ID of last access point queried
};

struct AccessPoint {
  //const char* ssid;
  char ssid[30];
  int rssi;
  char mac[18];
  int ch;
  bool active;
  String data; // TODO: Could really be changed to dataStore
};

RangingDevice rangingDevices[] = {
  {
    .device = HardwareSerial(1),
    .rx = 16, // 16
    .tx = 17 // 17
  }
  /*
  {
    .device = HardwareSerial(2),
    .rx = 18,
    .tx = 23
  }
  */
};

// This holds the final list of access points to query. Init to 1, change later.
//AccessPoint *accessPoints = (AccessPoint*) malloc(15*sizeof(AccessPoint));
AccessPoint accessPoints[15];
// That's not correct, but it's an easy test; everything above this means the SSIDs were received from the server.
int accessPointSize = -1;

char atTermination[] = "OK";

/********/
/* Time */
/********/
const char* timeNtpServer = "pool.ntp.org";
const long  timeGmtOffset_sec = 3600;
const int   timeDaylightOffset_sec = 0;

/*******/
/* IMU */
/*******/
#define SDA_PIN 21
#define SCL_PIN 22

int imuInterval = 50;

MPU9250 imu;

/**********/
/* Button */
/**********/
bool buttonDownLast = false;
bool buttonDownNow = false;
int buttonValue = 0;
