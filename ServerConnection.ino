bool serverConnectionEstablish() {

  connectionStatus = START;
  if (_serverConnectionWifiFindNetwork())
    if (_serverConnectionWifiConnect())
      if (_serverConnectionServerConnect())
        if (_serverConnectionWebsocketConnect()) {
          connectionStatus = WEBSOCKET_INITIALIZED;
          printStatus("Websocket initialized");
          return true;
        }
        
  return false;

}

void serverConnectionWifiRestart() {

  connectionStatus = START;
  if (wifiClient.connected()) {
    printStatus("Disconnecting server connection");
    wifiClient.stop();
  }
  else {
    printError("Not connected to server");
  }
  serverConnectionEstablish();
}

boolean _serverConnectionWifiFindNetwork() {
  
  // Don't do anything if we already have it.
  if (wifiAccessId != -1) {
    return true;
  }
  
  // Scan for available networks
  printStatus("Finding possible wifi networks to connect to.");
  int networksLength = WiFi.scanNetworks();
  String networks[networksLength];
  for (int i = 0; i < networksLength; i++) {
    networks[i] = WiFi.SSID(i);
  }

  // For each element in wifiAccess, check if available
  int wifiAccessLength = sizeof(wifiAccess)/sizeof(wifiAccess[0]);
  for(int i = 0; (i < wifiAccessLength) && (wifiAccessId == -1); i++) {
    for(int j = 0; j < networksLength; j++) {
      if (networks[j].equals(wifiAccess[i].ssid)) {
        wifiAccessId = i;
        break;
      }
    }
  }

  if (wifiAccessId == -1) {
    printError("No wifi network found!");
    return false;
  }
  else {
    printStatus("Network found:", wifiAccess[wifiAccessId].ssid);
    return true;
  }
 
}

boolean _serverConnectionWifiConnect() {
  
  if (WiFi.status() == WL_CONNECTED) {
    return true;
  }
  
  printStatus("Attempting to connect to wifi.");
  int counter = 0;
  if (strlen(wifiAccess[wifiAccessId].password) > 0) {
    WiFi.begin(wifiAccess[wifiAccessId].ssid, wifiAccess[wifiAccessId].password);
  }
  else {
    printStatus("...without password.");
    WiFi.begin(wifiAccess[wifiAccessId].ssid);
  }
  while (WiFi.status() != WL_CONNECTED && counter < wifiConnectionAttempts) {
    delay(wifiConnectionDelay);
    printStatus(". ", false);
    counter++;
  }

  if (WiFi.status() != WL_CONNECTED) {
    printError("Could not connect to wifi!");
    return false;
  }
  else {
    printBreak();
    printStatus("Wifi connected.");
    return true;
  }
}

boolean _serverConnectionServerConnect() {
  
  if (wifiClient.connected()) {
    return true;
  }

  for(int j=0; j<5; j++) {
    for (int i = 0; i < sizeof(localizationServers)/sizeof(localizationServers[0]); i++) {
      printStatus("Attempting to connect to server:", localizationServers[i].address);
      if (wifiClient.connect(localizationServers[i].address, localizationServers[i].port)) {
        localizationServerId = i;
        printStatus("Connected to:", localizationServers[i].address);
        return true;
      }
    }
  }

  if (!wifiClient.connected()) {
    printError("Could not connect to server!");
    return false;
  }
}

boolean _serverConnectionWebsocketConnect() {

  printStatus("Attempting handshake with websocket.");
  
  webSocketClient.path = (char *)localizationServers[localizationServerId].websocketPath;
  webSocketClient.host = (char *)localizationServers[localizationServerId].address;

  if (webSocketClient.handshake(wifiClient)) {
    printStatus("Handshake successful.");
    return true;
  } else {
    printError("Websocket handshake unsuccessful.");
    return false;
  }
}

