struct tm timeInfo;
int timeMsOffset = -1;

void timeGetTimestamp(char *timestamp) {
  
  if(!getLocalTime(&timeInfo)){
    Serial.println("Failed to obtain time");
    return;
  }

  int currentMs = timeMsOffset + millis();
  int hour = (int)floor(currentMs / 3600000);
  int minute = (int)floor((currentMs - (hour * 3600000)) / 60000);
  int second = (int)floor((currentMs - (hour * 3600000) - (minute * 60000)) / 1000);
  int ms = (int)floor(currentMs - (hour * 3600000) - (minute * 60000) - (second * 1000));
  sprintf(timestamp, "%04d%02d%02d%02d%02d%02d%03d", 1900+timeInfo.tm_year, 1+timeInfo.tm_mon, timeInfo.tm_mday, hour, minute, second, ms);
}


void timeConfig() {

  printStatus("Synchronizing time with Ntp Server");
  
  //init and get the time
  configTime(timeGmtOffset_sec, timeDaylightOffset_sec, timeNtpServer);

  // Get milliseconds
  getLocalTime(&timeInfo);
  timeMsOffset = timeInfo.tm_hour*3600000 + timeInfo.tm_min*60000 + timeInfo.tm_sec*1000;
}
