void imuInit() {
  Wire.begin(SDA_PIN, SCL_PIN);

  imu.setWire(&Wire);
  imu.beginAccel();
  imu.beginGyro();
  imu.beginMag();
}

void imuUpdate() {

  static int lastUpdate = -1;
  static float imuValue;

  if (millis() - lastUpdate > imuInterval) {
    lastUpdate = millis();
    
  
    char buf[10];
    char entry[30];

    imu.accelUpdate();
    
    snprintf(buf, sizeof buf, "%f", imu.accelX()*(-9.81));
    strcpy(entry, buf);
    Serial.println(buf);
  
    snprintf(buf, sizeof buf, "%f", imu.accelY()*(-9.81));
    strcat(entry, ",");
    strcat(entry, buf);
    
    snprintf(buf, sizeof buf, "%f", imu.accelZ()*(-9.81));
    strcat(entry, ",");
    strcat(entry, buf);

    Serial.println(entry);

    dataStoreAdd('a', entry);

    imu.gyroUpdate();
    
    snprintf(buf, sizeof buf, "%f", imu.gyroX());
    strcpy(entry, buf);

    snprintf(buf, sizeof buf, "%f", imu.gyroY());
    strcat(entry, ",");
    strcat(entry, buf);

    snprintf(buf, sizeof buf, "%f", imu.gyroZ());
    strcat(entry, ",");
    strcat(entry, buf);

    dataStoreAdd('g', entry);

    imu.magUpdate();

    snprintf(buf, sizeof buf, "%f", imu.magX());
    strcpy(entry, buf);
  
    snprintf(buf, sizeof buf, "%f", imu.magY());
    strcat(entry, ",");
    strcat(entry, buf);
    
    snprintf(buf, sizeof buf, "%f", imu.magZ());
    strcat(entry, ",");
    strcat(entry, buf);

    dataStoreAdd('m', entry, true);
  }
}
