struct dataStore {
  char a[38] = "";
  char g[38] = "";
  char m[38] = "";
  char r[38] = "";
};

String dataStoreData;
dataStore dataStoreEntry;

void dataStoreAdd(char type, char ap[30], char signalStrength[3]) {
  dataStoreAdd(type, ap, signalStrength, false);
}

void dataStoreAdd(char type, char ap[30], char signalStrength[3], bool last) {
  int dLength = strlen(ap) + 8;
  char d[dLength];
  strcpy(d, "\"");
  strcat(d, ap);
  strcat(d, "\":[");
  strcat(d, signalStrength);
  strcat(d, "]");

  dataStoreAdd(type, d, last);
}

void dataStoreAdd(char type, char d[38]) {
  dataStoreAdd(type, d, false);
}

void dataStoreAdd(char type, char d[38], bool last) {

  if (type == 'a') {
    strcpy(dataStoreEntry.a, d);
  }
  else if (type == 'g') {
    strcpy(dataStoreEntry.g, d);
  }
  else if (type == 'm') {
    strcpy(dataStoreEntry.m, d);
  }
  else if (type == 'r') {
    strcpy(dataStoreEntry.r, d);
  }
  
  if (last) {

    // Get time
    char timestamp[18];
    timeGetTimestamp(timestamp);

    // Add comma if not first
    if (dataStoreData.length()) {
      dataStoreData += ",";
    }

    // Timestamp
    dataStoreData += "{\"t\":";
    dataStoreData += timestamp;

    // Button
    dataStoreData += ",\"p\":";
    dataStoreData += buttonValue;
        
    if (strlen(dataStoreEntry.a)) {
      dataStoreData += ",\"a\":[";
      dataStoreData += dataStoreEntry.a;
      dataStoreData += "]";
    }

    if (strlen(dataStoreEntry.g)) {
      dataStoreData += ",\"g\":[";
      dataStoreData += dataStoreEntry.g;
      dataStoreData += "]";
    }

    if (strlen(dataStoreEntry.m)) {
      dataStoreData += ",\"m\":[";
      dataStoreData += dataStoreEntry.m;
      dataStoreData += "]";
    }
    
    if (strlen(dataStoreEntry.r)) {
      dataStoreData += ",\"r\":{";
      dataStoreData += dataStoreEntry.r;
      dataStoreData += "}";
    }

    dataStoreData += "}";

    // Reset Struct
    strcpy(dataStoreEntry.a, "");
    strcpy(dataStoreEntry.g, "");
    strcpy(dataStoreEntry.m, "");
    strcpy(dataStoreEntry.r, "");
  }
}

void dataStoreSend() {
  dataStoreSend('d');
}

void dataStoreSend(char type) {

  if (dataStoreData.length() > 0) {

    dataStoreData = "[" + dataStoreData;
    dataStoreData += "]";

    //Serial.println(dataStoreData);
  
    // Send
    serverCommunicationSend(dataStoreData, type);
  
    dataStoreData = "";
  }
  
  // Process
  // serverCommunicationProcess(); (Done in ServerCommunication)
  

}

bool dataStoreEmpty() {
  if (dataStoreData.length()) {
    return false;
  }
  else {
    return true;
  }
}

