/*
 * Read Signal Strengths
 * 
 * TODO: That's almost the same as in RangingMeasurements
 */
void rangingUpdateProcess() {

  static int apc = -1; // AccessPointCounter
  int devicesAmount = sizeof(rangingDevices)/sizeof(rangingDevices[0]);
  for(int i=0; i < devicesAmount; i++) {

    // I'm looking for the switch from busy = true to false to know when it's done.
    // Otherwise this wouldn't be necessary.
    if (rangingDevices[i].busy == true) {
      rangingRead(i);
      if (rangingDevices[i].busy == false) {
        int signalStrengthNum = rangingMeasurementsGetSignalStrength(rangingDevices[i].response);
        if (signalStrengthNum < 0) {
          char signalStrength[4];
          sprintf(signalStrength, "%d", signalStrengthNum);
          dataStoreAdd('r', accessPoints[rangingDevices[i].accessPointId].mac, signalStrength, true);
        }
      }
    }
    if (rangingDevices[i].busy == false) {

      do {
        apc++;
        if (apc >= accessPointSize) { apc = 0; }
      } while (accessPoints[apc].active == false);

      rangingDevices[i].accessPointId = apc;
      
      int cmdLength = 15 + strlen(accessPoints[apc].ssid) + strlen(accessPoints[apc].mac) + rangingGetDigits(accessPoints[apc].ch) + 1;
      char command[cmdLength+1];
      char channel[rangingGetDigits(accessPoints[apc].ch)];
      sprintf(channel, "%d", accessPoints[apc].ch);
      strcpy(command, "AT+CWLAP=\"");
      strcat(command, accessPoints[apc].ssid);
      strcat(command, "\",\"");
      strcat(command, accessPoints[apc].mac);
      strcat(command, "\",");
      strcat(command, channel);
      rangingWrite(i, command);
    }
  }  
}
