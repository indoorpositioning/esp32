void debugInit() {
  
  // Serial
  Serial.begin(baudRate);
}

void printStatus(const char *msg) {
  printStatus(msg, true);
}

void printStatus(const char *msg, bool linebreak) {
  if (printStatusMessages) {
    if (linebreak) {
      Serial.print("*** ");
      Serial.println(msg);
    }
    else {
      Serial.print(msg);
    }
  }
}

void printStatus(const char *msg1, const char *msg2) {
  if (printStatusMessages) {
    Serial.print("*** ");
    Serial.print(msg1);
    Serial.print(" ");
    Serial.println(msg2);
  }
}

void printStatus(const char *msg1, int msg2) {
  if (printStatusMessages) {
    Serial.print("*** ");
    Serial.print(msg1);
    Serial.print(" ");
    Serial.println(msg2);
  }
}

void printBreak() {
  if (printStatusMessages) {
    Serial.println();
  }
}

void printError(const char *msg) {
  if (printErrorMessages) {
    Serial.print("!!! ");
    Serial.println(msg);
  }
}

