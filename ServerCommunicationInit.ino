void serverCommunicationInit(AccessPoint *accessPoints, int deviceLength) {
  
  char start[] = "{\"location\":{\"type\":\"wifi\",\"devices\":[";
  char middle[] = "]},\"upload_id\": \"";
  char end[] = "\",\"type\":\"upload\",\"capabilities\":[\"wifi\"]}";

  int sizeRequirement = 0;
  for(int n=0; n<deviceLength; n++) {
    sizeRequirement += strlen(accessPoints[n].mac);
  }
  sizeRequirement += (deviceLength*2) + deviceLength-1 + 1; // Quotes and Commas
  char *devices = (char*) malloc(sizeRequirement);
  strcpy(devices, "\"");
  for(int n=0; n<deviceLength; n++) {
    strcat(devices, accessPoints[n].mac);
    if (n < deviceLength-1) {
      strcat(devices, "\",\"");
    } else {
      strcat(devices, "\"");
    }
  }

  char *json = (char*) malloc(strlen(start) + strlen(middle) + strlen(end) + strlen(identifier) + sizeRequirement); //+1 already in sizeRequirement
  strcpy(json, start);
  strcat(json, devices);
  strcat(json, middle);
  strcat(json, identifier);
  strcat(json, end);

  serverCommunicationSend(json, 'i');

  free(devices);
  free(json);
  
}

void serverCommunicationInitProcess() {
  if (accessPointSize == -1) {
    serverCommunicationProcess();
  }
  else {
    rangingFindAccessPoints();
  }
}

