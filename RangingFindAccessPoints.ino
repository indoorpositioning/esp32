/*
 * Get a structured list of all access points.
 * 
 * This is used to then access specifically those required.
 */
void rangingFindAccessPoints() {
  
  rangingRead(0);

  // Send command
  if (rangingDevices[0].busy == false && rangingDevices[0].status == WD_READY) {
    rangingDevices[0].status = WD_BUSY;
    rangingWrite(0, "AT+CWLAP");
    printStatus("AT+CWLAP sent");
  }

  // Command completed
  if (rangingDevices[0].busy == false && rangingDevices[0].status == WD_BUSY) {
    // Convert to AccessPoints
    int deviceLength = rangingGetAccessPointLength(0);
    AccessPoint allAccessPoints[deviceLength];
    rangingGetAccessPointData(0, allAccessPoints);

    /*
    int accessPointsAmount = sizeof(accessPoints)/sizeof(accessPoints[0]);
    for(int i=0; i < accessPointsAmount; i++) {
      Serial.println(accessPoints[i].ssid);
      Serial.println(accessPoints[i].mac);
      Serial.println("--------");
    }
    */

    if (accessPointSize == -1) {
      // Call it from here because that's where I have the accessPoints object
      serverCommunicationInit(allAccessPoints, deviceLength);
  
      rangingDevices[0].status = WD_READY;
      connectionStatus = SERVER_CONNECTION_INIT;
    }
    else {
      for(int i=0; i<accessPointSize; i++) {
        accessPoints[i].active = false;
        for(int j=0; j<deviceLength; j++) {
          if (strcmp(accessPoints[i].mac, allAccessPoints[j].mac) == 0) {
            strcpy(accessPoints[i].ssid, allAccessPoints[j].ssid);
            accessPoints[i].ch = allAccessPoints[j].ch;
            accessPoints[i].active = true;
            break;
          }
        }
      }

      connectionStatus = READY;
      initFinished = true;
    }
  }

}

int rangingGetAccessPointLength(int i) {
  int len = rangingDevices[i].response.length();
  int count = 0;
  for(int j=0; j<len; j++) {
    if (rangingDevices[i].response.charAt(j) == '+') { count++; }
  }
  return count;
}

void rangingGetAccessPointData(int i, AccessPoint *accessPoints) {

  // This was originally done for a char array (not String), so... convert.
  int len = rangingDevices[i].response.length()+1;
  char list[len];
  rangingDevices[i].response.toCharArray(list, len);

  char *string = list;
  char *token  = strchr(string, '\n');

  int col = 0;
  int row = 0;
  while (token != NULL) {

    *token++ = '\0';
    if (strlen(string) > 20) {
      string = rangingTrimToData(string);
      
      char *token2 = strtok(string, ",");
      col = 0;
      while (token2 != NULL) {
          token2 = rangingTrimField(token2);

          if (col == 1) {
            strcpy(accessPoints[row].ssid, token2);
          }
          else if (col == 2) {
            accessPoints[row].rssi = atoi(token2);
          }
          else if (col == 3) {
            strcpy(accessPoints[row].mac, token2);
          }
          else if (col == 4) {
            accessPoints[row].ch = atoi(token2);
          }
          token2 = strtok(NULL, ",");
          col++;
      }
      row++;
    }
    string = token;
    token = strchr(string, '\n');
  }
}

char* rangingTrimField(char* str) {

  // Trim leading space
  while(isspace((unsigned char)*str) || (unsigned char)*str == '"') str++;

  // Trim trailing space
  char *end = str + strlen(str) - 1;
  while(end > str && (isspace((unsigned char)*end) || (unsigned char)*end == '"')) end--;

  str[end-str+1] = '\0';
  
  return str;
}

char* rangingTrimToData(char* str) {

  // Remove everything up to leading (
  while ((unsigned char)*str != '(') str++;
  str++;

  // Trim trailing space
  char *end = str + strlen(str) - 1;
  while(end > str && (unsigned char)*end != ')') end--;
  end--;

  str[end-str+1] = '\0';
  
  return str;
}
